package org.crossref.refmatching;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Dominika Tkaczyk
 */
public class ReferenceTest {
    
    @Test
    public void testBasicUnstructured() {
        Reference ref = new Reference("formatted string");
        
        Assert.assertEquals(ReferenceType.UNSTRUCTURED, ref.getType());
        Assert.assertEquals("formatted string", ref.getFormattedString());
        Assert.assertNull(ref.getFieldValue("year"));
    }

    @Test
    public void testBasicStructured() {
        Map<String, String> map = new HashMap<>();
        map.put("author", "West");
        map.put("volume", "84");
        map.put("first-page", "1763");
        map.put("year", "1962");
        map.put("journal-title", "J. Amer. chem. Soc.");
        
        Reference ref = new Reference(map);
        
        Assert.assertEquals(ReferenceType.STRUCTURED, ref.getType());
        Assert.assertNull(ref.getFormattedString());
        
        Assert.assertNull(ref.getFieldValue("article-title"));
        Assert.assertNull(ref.getFieldValue("issue"));
        map.keySet().forEach((key) -> {
            Assert.assertEquals(map.get(key), ref.getFieldValue(key));
        });
        
        JSONObject json = new JSONObject(map);
        Reference ref2 = new Reference(json);
        Assert.assertEquals(ReferenceType.STRUCTURED, ref2.getType());
        Assert.assertNull(ref2.getFormattedString());
        
        Assert.assertNull(ref2.getFieldValue("article-title"));
        Assert.assertNull(ref2.getFieldValue("issue"));
        map.keySet().forEach((key) -> {
            Assert.assertEquals(map.get(key), ref2.getFieldValue(key));
        });
    }
    
    @Test
    public void testWithField() {
        Map<String, String> map = new HashMap<>();
        map.put("author", "West");
        map.put("volume", "84");
        map.put("first-page", "1763");
        map.put("year", "1962");
        map.put("journal-title", "J. Amer. chem. Soc.");
        
        Reference ref = new Reference(map);
        Reference refModified =
                ref.withField("issue", "13").withField("author", "South");
        
        Assert.assertEquals("1962", refModified.getFieldValue("year"));
        Assert.assertEquals("13", refModified.getFieldValue("issue"));
        Assert.assertEquals("South", refModified.getFieldValue("author"));
    }
    
    @Test
    public void testGetMetadata() {
        Map<String, String> map = new HashMap<>();
        map.put("author", "West");
        map.put("volume", "84");
        map.put("first-page", "1763");
        map.put("year", "1962");
        map.put("journal-title", "J. Amer. chem. Soc.");
        
        Reference ref = new Reference(map);
        
        Assert.assertEquals(map, ref.getMetadataAsMap());
        Assert.assertTrue(ref.getMetadataAsJSON().similar(new JSONObject(map)));
    }
    
}